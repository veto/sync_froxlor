#!/bin/bash

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
source $parent_path/config.cfg
echo $parent_path
echo $database
if [ -z ${email_list} ] 
then
  echo "...get emails from froxlor database"
  #echo "database: ${database}"
  #echo "source_host: ${source_host}"
  #echo "user: ${user}"
  #echo "password: ${source_password}"
  #echo "password: ${source_password}"
  echo "host: ${source_host}"  
  echo "target: ${target_host}"
  if [ -z ${domain} ] 
  then  
      emails=$(docker run -i --rm salamander1/mysql $database -h $source_host -u $user -p$source_password<<<"SELECT concat(username,'|', password) as email FROM $table WHERE username != '' and password != ''")
  else
      emails=$(docker run -i --rm salamander1/mysql $database -h $source_host -u $user -p$source_password<<<"SELECT concat(username,'|', password) as email FROM $table WHERE username != '' and password != '' and email like '%@${domain}'")      
  fi    
else
  echo "...use config emails"
  emails=$email_list
fi


for s in $emails; do
    IFS='|' read -a arr <<< "$s"
    if [ -z ${arr[1]} ]
    then
	echo "no action"
    else	
      echo "email: ${arr[0]}"
      echo "pass: ${arr[1]}"
      docker run -i --rm salamander1/imapsync --sep1 . --prefix1 '' --sep2 .  --host1 $source_host --user1 ${arr[0]}  --password1 ${arr[1]}  --host2 $target_host --user2 ${arr[0]} --password2 ${arr[1]}
   fi   
done    

